# OpenML dataset: frenchopen-men-2013

https://www.openml.org/d/42969

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Shruti Jauhari, Aniket Morankar, Ernest Fokoue
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Tennis+Major+Tournament+Match+Statistics) - 2014
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**Tennis Major Tournament Match Statistics - French Open Men 2013**

This is a part of collection of 8 files containing the match statistics for both women and men at the four major tennis tournaments of the year 2013. Each file has 42 columns and a minimum of 76 rows. This file contains the statistics for the French Open Men's Edition for 2013.

### Attribute information

- Player 1 Name of Player 1 
- Player 2 Name of Player 2 
Result Result of the match (0/1) - Referenced on Player 1 is Result = 1 if Player 1 wins (FNL.1>FNL.2) 
- FSP.1 First Serve Percentage for player 1 (Real Number) 
- FSW.1 First Serve Won by player 1 (Real Number) 
- SSP.1 Second Serve Percentage for player 1 (Real Number) 
- SSW.1 Second Serve Won by player 1 (Real Number) 
- ACE.1 Aces won by player 1 (Numeric-Integer) 
- DBF.1 Double Faults committed by player 1 (Numeric-Integer) 
- WNR.1 Winners earned by player 1 (Numeric) 
- UFE.1 Unforced Errors committed by player 1 (Numeric) 
- BPC.1 Break Points Created by player 1 (Numeric) 
- BPW.1 Break Points Won by player 1 (Numeric) 
- NPA.1 Net Points Attempted by player 1 (Numeric) 
- NPW.1 Net Points Won by player 1 (Numeric) 
- TPW.1 Total Points Won by player 1 (Numeric) 
- ST1.1 Set 1 result for Player 1 (Numeric-Integer) 
- ST2.1 Set 2 Result for Player 1 (Numeric-Integer) 
- ST3.1 Set 3 Result for Player 1 (Numeric-Integer) 
- ST4.1 Set 4 Result for Player 1 (Numeric-Integer) 
- ST5.1 Set 5 Result for Player 1 (Numeric-Integer) 
- FNL.1 Final Number of Games Won by Player 1 (Numeric-Integer) 
- FSP.2 First Serve Percentage for player 2 (Real Number) 
- FSW.2 First Serve Won by player 2 (Real Number) 
- SSP.2 Second Serve Percentage for player 2 (Real Number) 
- SSW.2 Second Serve Won by player 2 (Real Number) 
- ACE.2 Aces won by player 2 (Numeric-Integer) 
- DBF.2 Double Faults committed by player 2 (Numeric-Integer) 
- WNR.2 Winners earned by player 2 (Numeric) 
- UFE.2 Unforced Errors committed by player 2 (Numeric) 
- BPC.2 Break Points Created by player 2 (Numeric) 
- BPW.2 Break Points Won by player 2 (Numeric) 
- NPA.2 Net Points Attempted by player 2 (Numeric) 
- NPW.2 Net Points Won by player 2 (Numeric) 
- TPW.2 Total Points Won by player 2 (Numeric) 
- ST1.2 Set 1 result for Player 2 (Numeric-Integer) 
- ST2.2 Set 2 Result for Player 2 (Numeric-Integer) 
- ST3.2 Set 3 Result for Player 2 (Numeric-Integer) 
- ST4.2 Set 4 Result for Player 2 (Numeric-Integer) 
- ST5.2 Set 5 Result for Player 2 (Numeric-Integer) 
- FNL.2 Final Number of Games Won by Player 2 (Numeric-Integer) 
- Round Round of the tournament at which game is played (Numeric-Integer)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42969) of an [OpenML dataset](https://www.openml.org/d/42969). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42969/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42969/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42969/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

